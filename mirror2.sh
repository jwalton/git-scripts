#!/bin/sh

# Generic svn->git mirroring script

set -e

AUTHORS="$HOME/svn-authors"
BASE="`dirname "$0"`"

[ -f mirrors ] || { echo "Unable to find file 'mirrors'"; exit 10; }

while true; do
 while read dir svnurl giturl; do
  echo "Dir: $dir"
  echo "SVN: $svnurl"
  echo "Git: $giturl"

  if [ ! -d "$dir" ]; then
   git svn clone --stdlayout --no-metadata --authors-file "$AUTHORS" "$svnurl" "$dir"
   "$BASE/"annotate-tags.sh
  fi

  cd "$dir"
  git svn fetch

  TOPUSH="remotes/trunk:refs/heads/master"
  while read b && [ -n "$b" ]; do
   TOPUSH="$TOPUSH remotes/$b:refs/heads/$b"
  done

  git push $giturl $TOPUSH

  cd ..
 done <$HOME/mirror/mirrors

 sleep 2m
done
