#!/bin/bash

# To push all historic branches to an archive:
#  for b in $(../git-scripts/very-old-branches.sh ); do git push archive origin/$b:refs/heads/$b; done

# To delete branches from current development:
#  for b in $(../git-scripts/very-old-branches.sh ); do git push --delete origin refs/heads/$b; done


# How long ago is a branch considered live?
since="$(( $(date +%s) - ( 60 * 60 * 24 * 30 * 1 ) )) "

echo >&2 "Since $(date -r $since)"

git for-each-ref --format='%(committerdate:raw) %(refname)' refs/remotes/origin/ |
  fgrep -v _stable | # Exclude branches tagged as '_stable'

  while read stamp tz refname; do
    if [ "$stamp" -lt "$since" ]; then
      echo "$refname"
    fi
  done |
  sed -e 's,refs/remotes/origin/,,' -e 's, .*,,'
