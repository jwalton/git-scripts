#!/bin/bash

set -e

# For every remote 'tag/' branch...
git for-each-ref --format='%(refname)' refs/remotes/tags/\* |
while read tag_ref; do
  tag=${tag_ref#refs/remotes/tags/}
  tree=$( git rev-parse "$tag_ref": )

  # If the parent has an identical tree, create a new annotated tag
  #  with the details from this commit
  parent_ref="$tag_ref";
  if [ $( git rev-parse --quiet --verify "$parent_ref"^: ) = "$tree" ]; then
    target_ref="$parent_ref^"
    echo "$tag $target_ref"

    git show -s --pretty='format:%s%n%n%b' "$tag_ref" |
    perl -ne 'next if /^git-svn-id:/; $s++, next if /^\s*r\d+\@.*:.*\|/; s/^ // if $s; print' |
    env GIT_COMMITTER_NAME="$( git show -s --pretty='format:%an' "$tag_ref" )" \
        GIT_COMMITTER_EMAIL="$( git show -s --pretty='format:%ae' "$tag_ref" )" \
        GIT_COMMITTER_DATE="$( git show -s --pretty='format:%ad' "$tag_ref" )" \
        git tag -a -F - "$tag" "$target_ref"

    #... and get rid of the branch
    git branch -Dr "tags/$tag"
  fi
done
