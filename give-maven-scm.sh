#!/bin/bash

echo "    <scm>"
echo "        <connection>scm:git:git@bitbucket.org:$1.git</connection>"
echo "        <developerConnection>scm:git:git@bitbucket.org:$1.git</developerConnection>"
echo "        <url>https://bitbucket.org/$1</url>"
echo "    </scm>"
