#!/bin/bash

if [ -z "$1" ]; then
    echo "No argument specified.  Format is (with an actual slash) $0 accountname/reponame"
    exit 0
fi

svn delete * 
echo "Source has moved to https://bitbucket.org/$1" > README.TXT
svn add README.TXT 
svn commit . -m "Source migrated to https://bitbucket.org/$1"
