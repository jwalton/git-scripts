#!/usr/bin/perl -w

use strict;

# Resolve merge conflicts due to changing development versions in
#  different branches.

# In git:
# git config mergetool.versions.cmd '$HOME/source/git-scripts/resolve-maven-version-conflicts.pl <"$MERGED" >"$MERGED.tmp" && mv "$MERGED.tmp" "$MERGED"'
# git config mergetool.versions.trustExitCode true
#
# then merge with
# git mergetool --tool=versions -y

# Does a fragment include only a version tag with a snapshot version?
sub justASnapshotVersion($)
{
 return $_[0] =~ /^\s*<version>.+-SNAPSHOT<\/version>\s*$/s;
}


my @lines;

while (<>) {
 push @lines, $_;
}

# print "Lines: ", scalar(@lines), "\n";

my $clean = 1;

for (my $l = 0; $l < @lines; $l++) {
 if ($lines[$l] =~ /^<<<<<<< (.+)$/) {
  my $m = $l + 1;
  while ($lines[$m] !~ /^=======\r?$/) {
   $m++;
  }

  my $n = $m + 1;
  while ($lines[$n] !~ /^>>>>>>> (.+)$/) {
   $n++;
  }

  my ($from) = $lines[$l] =~ /^<<<<<<< (.+)$/;
  my ($to) = $lines[$n] =~ /^>>>>>>> (.+)$/;

#  print "From: $from, to: $to\n";

#  print "$l $m $n\n";
  my $fromBody = join('', @lines[($l + 1)..($m - 1)]);
  my $toBody = join('', @lines[($m + 1)..($n - 1)]);

#  print STDERR $fromBody;
#  print STDERR $toBody;

  if (justASnapshotVersion($fromBody) && justASnapshotVersion($toBody)) {
   print $fromBody;
  } else {
   print join('', @lines[$l..$n]);
   $clean = 0;
  }

  $l = $n;
 } else {
  print $lines[$l];
 }
}

if ($clean) {
  exit 0;
} else {
  exit 1
}
