#!/bin/sh

# Find unused branches: the only commit is branch
#  creation off master  and there are no changes to files.

set -e

git for-each-ref --format='%(refname)' 'refs/remotes/*/*' |
while read br; do
  branchpoint="`git merge-base master $br`"

  # There's only one commit in the branch
  if [ "$branchpoint" = "`git rev-parse $br^`" ]; then
    # Do the files change?
    trunktree="`git rev-parse --verify "$branchpoint:"`"
    branchtree="`git rev-parse --verify "$br:"`"

    if [ "$trunktree" = "$branchtree" ]; then
      echo $br
    fi
    
  fi
done
