#!/bin/sh

set -e

# A local git-svn clone
cd /home/jwalton/fedex/crowd-mirror-2

REV=""

while git svn fetch; do
 NREV="`git rev-parse trunk`"

 # If something's changed in svn, push it to the remote Git repo
 if [ "$REV" != "$NREV" ]; then
  git push origin trunk:master remotes/2_3_stable:refs/heads/2_3_stable
  REV="$NREV"
 else
  echo "No changes: $REV == $NREV"
 fi

 sleep 5m
done
